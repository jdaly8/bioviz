BioViz.org Web content - version-controlled for easy deployment.

### About ###

The site serves three functions:

* Download site for Integrated Genome Browser, target for automatic build process 
* Provides javascript bridge for Galaxy users (see galaxy.html)
* Links to IGB documentation, including users guide, developers guide, etc

### Setting up ###

* Install Apache.
* Clone repository.
* Edit httpd.conf: Set DocumentRoot to htdocs directory

### Contact ###

* Ann Loraine